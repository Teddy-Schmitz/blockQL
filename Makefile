.PHONY: graph

graph:
	go-bindata \
		-prefix schema \
		-pkg schema -o schema/schema.go \
		schema