FROM golang:1.9

ADD blockQL /go/bin/
ENTRYPOINT [ "/go/bin/blockQL" ]
EXPOSE 5050