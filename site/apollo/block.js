import gql from 'graphql-tag';

export const recentInfo = gql`
  {
    blockchain {
      recentBlocks {
        hash 
        height 
        difficulty
        time
        transactions { 
          id 
          value 
        }
      }
    }
  }
`;

export const networkInfo = gql`
  {
    blockchain {
      networkHash
    }
  }
`;

export const priceInfo = gql`
  {
    blockchain {
      price
    }
  }
`;

export const blockdetails = gql`
query ($hash: String!) {
  block(hash: $hash) {
    size
    height
    confirmations
    time
    difficulty
    size
    nonce
    weight
    version
    chainwork
    transactions {
      id
      value
      hash
      size
    }
  }
}
`;

export const transactionDetails = gql`
query ($hash: String!) {
  transaction(id: $hash) {
		blockhash
		size
		value
		block {
		  confirmations
		  time
      height
		}	
  }
}`;

export const trxVIN = gql`
query ($hash: String!, $cursor: String) {
  transaction(id: $hash) {
    vin(after: $cursor) {
      count
      edges {
        cursor
        node {
          coinbase
          value
          addresses
          previousTransactionID
        }
      }
    }
  }
}`;

export const trxVOut = gql`
query ($hash: String!, $cursor: String) {
  transaction(id: $hash) {
    vout(after: $cursor) {
      count
      edges {
        cursor
        node {
          value
          index
          address
        }
      }
    }
  }
}`;

export const addressDetails = gql`
query ($hash: String!) {
	address(address: $hash) {
	  address
		value
		transactions {
			count
		}
	}
}`;

export const addressSpends = gql`
query ($hash: String!, $cursor:String!) {
	address(address: $hash) {
		 spends(after: $cursor){
			count
			edges {
				cursor
				node {
				  value					
					transaction {
						id
						hash
						time
					}
				}
			}
		}
	}
}
`;

export const addressReceives = gql`
query ($hash: String!, $cursor:String!) {
	address(address: $hash) {
		 receives(after: $cursor){
			count
			edges {
				cursor
				node {
				  value					
					transaction {
						id
						hash
						time
					}
				}
			}
		}
	}
}
`;

export const blocks = gql`
query ($cursor: String!) {
  blocks(after: $cursor) {
		count
		edges {
			node {
				hash
				height
				time
			}
			cursor
		}
	}
}
`;

export const transactions = gql`
query ($cursor: String!) {
  transactions(after: $cursor) {
      count
      edges {
        cursor
        node {
          id
          time
          value
      }
    }
  }
}
`;

export const search = gql`
query ($hash: String, $height: UInt64, $id: String, $address: String) {
  search(hash: $hash, height: $height, id: $id, address: $address){
		blocks {
			hash
			height
			time
		}
		transactions{
			id
			time
			value
		}
		addresses {
			address
		}
	}
}
`;

export const richlist = gql`
{
  blockchain {
    richlist {
      address
      rank
      value
    }
  }
}
`;
