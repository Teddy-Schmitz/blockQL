export function getValueByPath(obj, path) {
  const value = path.split('.').reduce((o, i) => o[i], obj)
  return value
}
