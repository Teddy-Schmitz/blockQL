package graph

import (
	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	"github.com/neelance/graphql-go"
	"github.com/neelance/graphql-go/relay"
	"git.egnomatics.com/jdioutkast/blockQL/schema"
)

func RegisterGraphQLRoutes(router *mux.Router, db *sqlx.DB) {
	data, err := schema.Asset("schema.gql")
	if err != nil {
		logrus.WithError(err).Fatal("error reading schema file")
	}

	gql, err := graphql.ParseSchema(string(data), &resolver{db: db})
	if err != nil {
		logrus.WithError(err).Fatal("error parsing schema")
	}

	router.Handle("", &relay.Handler{Schema: gql})
}
