package graph

import (
	"github.com/jmoiron/sqlx"
	sq "github.com/Masterminds/squirrel"
	"fmt"
)

type searchConnection struct {
	db *sqlx.DB

	args *searchArgs
}

func (b *searchConnection) Blocks() (*[]*block, error) {

	q := sq.Select("*").From("blocks")
	s := sq.Or{}

	if b.args.Height != nil {
		s = append(s, sq.Eq{"height": b.args.Height})
	}

	if b.args.Hash != nil {
		s = append(s, sq.Eq{"hash": b.args.Hash})
	}

	query, arg, _ := q.Where(s).PlaceholderFormat(sq.Dollar).ToSql()

	var blocks []*dbBlock
	if err := b.db.Select(&blocks, query, arg...); err != nil {
		return nil, err
	}

	var edges []*block
	for _, bl := range blocks {
		edges = append(edges, &block{db: b.db, block: bl})
	}

	return &edges, nil
}

func (b *searchConnection) Transactions() (*[]*transaction, error) {

	q := sq.Select("*").From("transactions")
	s := sq.Or{}
	if b.args.ID != nil {
		s = append(s, sq.Eq{"id": b.args.ID})
	}

	if b.args.Hash != nil {
		s = append(s,sq.Eq{"hash": b.args.Hash})
	}

	query, arg, _ := q.Where(s).PlaceholderFormat(sq.Dollar).ToSql()

	var trxs []*dbTransaction
	if err := b.db.Select(&trxs, query, arg...); err != nil {
		return nil, err
	}

	var edges []*transaction
	for _, trx := range trxs {
		edges = append(edges, &transaction{db: b.db, trx: trx})
	}

	return &edges, nil
}

func (b *searchConnection) Addresses() (*[]*address, error) {
	var addresses []*address
	if b.args.Address == nil || len(*b.args.Address) < 3 {
		return &addresses, nil
	}

	q := sq.Select("distinct address").From("transaction_out").Where("address ILIKE ?", fmt.Sprint("%",*b.args.Address,"%"))
	//q := sq.Select("1").From("transaction_out txo").Where("txo.addresses @> (ARRAY[?])", b.args.Address)
	query, arg, _ := q.PlaceholderFormat(sq.Dollar).ToSql()

	var res []*struct {
		Address string
	}

	if err := b.db.Select(&res, query, arg...); err != nil {
		return nil, err
	}

	for _, add := range res {
		addresses = append(addresses, &address{db: b.db, address: add.Address})
	}

	return &addresses, nil
}

func (b *searchConnection) PageInfo() (*PageInfo) {

	return &PageInfo{
		hasprevious: false,
		hasnext: true,
	}
}

