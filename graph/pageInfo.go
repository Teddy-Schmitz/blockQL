package graph

type PageInfo struct {
	hasnext bool
	hasprevious bool
}

func (p *PageInfo) HasPreviousPage() bool {
	return p.hasprevious
}

func (p *PageInfo) HasNextPage() bool {
	return p.hasnext
}
