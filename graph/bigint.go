package graph

import "fmt"

type Int64 int64

func (_ Int64) ImplementsGraphQLType(name string) bool {
	return name == "Int64"
}

func (b *Int64) UnmarshalGraphQL(input interface{}) error {
	switch input := input.(type) {
	case int32:
		*b = Int64(input)
	case int:
		*b = Int64(input)
	case int64:
		*b = Int64(input)
	default:
		return fmt.Errorf("wrong type")
	}

	return nil
}

type UInt64 uint64

func (_ UInt64) ImplementsGraphQLType(name string) bool {
	return name == "UInt64"
}

func (b *UInt64) UnmarshalGraphQL(input interface{}) error {
	switch input := input.(type) {
	case int32:
		*b = UInt64(input)
	case int:
		*b = UInt64(input)
	case uint64:
		*b = UInt64(input)
	default:
		return fmt.Errorf("wrong type")
	}

	return nil
}