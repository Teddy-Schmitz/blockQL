package graph

import (
	"github.com/jmoiron/sqlx"
	sq "github.com/Masterminds/squirrel"
	"database/sql"
	"github.com/lib/pq"
)

type vIn struct {
	db *sqlx.DB

	trx *dbVin
}

type dbVin struct {
	UID int64 `json:"uid" db:"uid"`
	Coinbase sql.NullString `json:"coinbase,omitempty" db:"coinbase"`
	Sequence int64 `json:"sequence" db:"sequence"`
	PreviousTransactionID sql.NullString `json:"txid,omitempty" db:"previous_transaction_id"`
	OutIndex *sql.NullInt64 `json:"vout" db:"out_index"`
	ASM sql.NullString `json:"asm,omitempty" db:"asm"`
	HEX sql.NullString `json:"hex,omitempty" db:"hex"`
	TransactionID string `json:"transaction_id" db:"transaction_id"`
}

func (d *vIn) UID() Int64 {
	return Int64(d.trx.UID)
}

func (d *vIn) Coinbase() *string {
	if !d.trx.Coinbase.Valid {
		return nil
	}
	return &d.trx.Coinbase.String
}

func (d *vIn) PreviousTransactionID() *string {
	return &d.trx.PreviousTransactionID.String
}

func (d *vIn) Sequence() Int64 {
	return Int64(d.trx.Sequence)
}

func (d *vIn) OutIndex() *Int64 {
	if !d.trx.OutIndex.Valid {
		return nil
	}
	t := Int64(d.trx.OutIndex.Int64)
	return &t
}

func (d *vIn) ASM() *string {
	if !d.trx.ASM.Valid {
		return nil
	}
	return &d.trx.ASM.String
}

func (d *vIn) HEX() *string {
	if !d.trx.HEX.Valid {
		return nil
	}
	return &d.trx.HEX.String
}

func (d *vIn) TransactionID() string {
	return d.trx.TransactionID
}

func (d *vIn) Value() (*string, error) {

	if d.Coinbase() != nil {
		return nil, nil
	}

	q := sq.Select("value").From("transaction_out").Where(sq.Eq{
		"transaction_id": d.PreviousTransactionID(),
		"index": d.OutIndex(),
	})
	query, arg, _ := q.PlaceholderFormat(sq.Dollar).ToSql()

	var res string

	err := d.db.Get(&res, query, arg...)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
	}
	return &res, err
}

func (d *vIn) Addresses() (*[]string, error) {

	if d.Coinbase() != nil {
		return nil, nil
	}

	q := sq.Select("array_agg(distinct address)").From("transaction_out").Where(sq.Eq{
		"transaction_id": d.PreviousTransactionID(),
		"index": d.OutIndex(),
	})
	query, arg, _ := q.PlaceholderFormat(sq.Dollar).ToSql()

	var res pq.StringArray
	err := d.db.Get(&res, query, arg...)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
	}

	t := []string(res)
	return &t, err
}

func (d *vIn) Transaction() (*transaction, error) {
	q := sq.Select("*").From("transactions").Where(sq.Eq{"id": d.TransactionID()})
	trx := &dbTransaction{}
	query, arg, _ := q.PlaceholderFormat(sq.Dollar).ToSql()

	if err := d.db.Get(trx, query, arg...); err != nil {
		return nil ,err
	}

	return &transaction{
		db: d.db,
		trx: trx,
	}, nil
}

func (d *vIn) PreviousTransaction() (*transaction, error) {
	if d.PreviousTransactionID() == nil {
		return nil, nil
	}

	q := sq.Select("*").From("transactions").Where(sq.Eq{"id": d.PreviousTransactionID()})
	trx := &dbTransaction{}
	query, arg, _ := q.PlaceholderFormat(sq.Dollar).ToSql()

	if err := d.db.Get(trx, query, arg...); err != nil {
		return nil ,err
	}

	return &transaction{
		db: d.db,
		trx: trx,
	}, nil
}