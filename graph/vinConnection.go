package graph

import (
	"github.com/jmoiron/sqlx"
	"encoding/base64"
	"fmt"
	"github.com/Masterminds/squirrel"
)

type VInEdge struct {
	db *sqlx.DB

	trx *vIn
}

func (b *VInEdge) Node() *vIn {
	return b.trx
}

func (b *VInEdge) Cursor() string {
	return base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%v", b.trx.UID())))
}

type vinConnection struct {
	db *sqlx.DB

	args *trxsArgs
	query squirrel.SelectBuilder
}

func (b *vinConnection) Count() (int32, error){
	query, arg, _ := squirrel.Select("count(uid)").
		FromSelect(b.query, "x").PlaceholderFormat(squirrel.Dollar).ToSql()

		var count int32
		if err := b.db.Get(&count, query, arg...); err != nil {
			return 0, err
		}
		return count, nil
}

func (b *vinConnection) Edges() (*[]*VInEdge, error) {

	q := b.query.OrderBy("txi.uid")

	if b.args.After != nil {
		h, err := base64.StdEncoding.DecodeString(*b.args.After)
		if err != nil {
			return nil, err
		}
		q = q.Where(squirrel.GtOrEq{"txi.uid": string(h)})
	}

	query, arg, _ := q.Limit(uint64(b.args.First)).
		PlaceholderFormat(squirrel.Dollar).ToSql()

	var trxs []*dbVin
	if err := b.db.Select(&trxs, query, arg...); err != nil {
		return nil, err
	}

	var edges []*VInEdge
	for _, trx := range trxs {
		edges = append(edges, &VInEdge{db: b.db, trx: &vIn{
			trx: trx,
			db: b.db,
		}})
	}

	return &edges, nil
}

func (b *vinConnection) PageInfo() (*PageInfo) {

	return &PageInfo{
		hasprevious: false,
		hasnext: true,
	}
}

