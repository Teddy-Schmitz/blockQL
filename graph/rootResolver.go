package graph

import (
	"github.com/jmoiron/sqlx"
	"context"

	sq "github.com/Masterminds/squirrel"
)

type resolver struct {
	db *sqlx.DB
}

type blockArgs struct {
	Hash *string
	Height *int32
}

func (r *resolver) Block(ctx context.Context, args blockArgs) (*block, error) {
	q := sq.Select("*").From("blocks")

	if args.Hash != nil {
		q = q.Where(sq.Eq{"hash": *args.Hash})
	}

	if args.Height != nil {
		q = q.Where(sq.Eq{"height": *args.Height})
	}

	dblock := &dbBlock{}

	query, arg, _ := q.PlaceholderFormat(sq.Dollar).ToSql()

	if err := r.db.GetContext(ctx, dblock, query, arg...); err != nil {
		return nil, err
	}

	return &block{
		db: r.db,
		block: dblock,
	}, nil
}

type trxArgs struct {
	ID *string
	Hash *string
}

func (r *resolver) Transaction(ctx context.Context, args trxArgs) (*transaction, error) {
	q := sq.Select("*").From("transactions")

	if args.ID != nil {
		q = q.Where(sq.Eq{"id": *args.ID})
	}

	if args.Hash != nil {
		q = q.Where(sq.Eq{"hash": *args.Hash})
	}

	trx := &dbTransaction{}
	query, arg, _ := q.PlaceholderFormat(sq.Dollar).ToSql()

	if err := r.db.GetContext(ctx, trx, query, arg...); err != nil {
		return nil ,err
	}

	return &transaction{
		db: r.db,
		trx: trx,
	}, nil
}

type addressArgs struct {
 Address string
}
func (r *resolver) Address(ctx context.Context, args addressArgs) (*address, error) {
	return &address{
		db: r.db,
		address: args.Address,
	}, nil
}

type blockchainArgs struct {
	Limit int32
}

func (r *resolver) Blockchain(ctx context.Context, args blockchainArgs) (*blockchainConnection, error) {
	return &blockchainConnection{
		db: r.db,
		args: &args,
	}, nil
}

type blocksArgs struct {
	First int32
	After *string
}

func (r *resolver) Blocks(ctx context.Context, args blocksArgs) (*blockConnection, error) {

	q := sq.Select("*").From("blocks").OrderBy("height")
	return &blockConnection{
		db: r.db,
		query: q,
		args: &args,
	}, nil
}

func (r *resolver) Transactions(ctx context.Context, args trxsArgs) (*transactionConnection, error) {

	q := sq.Select("*").From("transactions").OrderBy("uid")
	return &transactionConnection{
		db: r.db,
		query: q,
		args: &args,
	}, nil
}

type searchArgs struct {
	Hash *string
	Height *int32
	ID *string
	Address *string
}

func (r *resolver) Search(ctx context.Context, args searchArgs) (*searchConnection, error) {
	return &searchConnection{
		db: r.db,
		args: &args,
	}, nil
}