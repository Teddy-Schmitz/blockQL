package graph

import (
	"github.com/jmoiron/sqlx"
	"github.com/Masterminds/squirrel"
	"fmt"
	"github.com/neelance/graphql-go"
	"time"
)

type block struct {
	db *sqlx.DB

	block *dbBlock
}

type dbBlock struct {
	UID int64 `json:"uid" db:"uid"`
	Hash string `json:"hash" db:"hash"`
	StrippedSize uint64 `json:"strippedsize" db:"stripped_size"`
	Size uint64 `json:"size" db:"size"`
	Weight uint64 `json:"weight" db:"weight"`
	Height uint64 `json:"height" db:"height"`
	Version uint64 `json:"version" db:"version"`
	VersionHex string `json:"versionHex" db:"version_hex"`
	MerkleRoot string `json:"merkleroot" db:"merkle_root"`
	Time int64 `json:"time" db:"time"`
	MedianTime int64 `json:"mediantime" db:"median_time"`
	Nonce int64 `json:"nonce" db:"nonce"`
	Bits string `json:"bits" db:"bits"`
	Difficulty float32 `json:"difficulty" db:"difficulty"`
	Chainwork string `json:"chainwork" db:"chainwork"`
	PreviousBlockhash string `json:"previousblockhash" db:"previous_blockhash"`
	NextBlockhash string `json:"nextblockhash" db:"next_blockhash"`
	//Transactions Transactions `json:"tx" db:"-"`
}

func (d *block) Hash() string {
	return d.block.Hash
}

func (d *block) UID() Int64 {
	return Int64(d.block.UID)
}

func (d *block) Confirmations() (UInt64, error) {
	var con UInt64
	q := squirrel.Select(fmt.Sprintf("max(height) - %v", d.Height())).From("blocks")
	query, _, _ := q.ToSql()
	err := d.db.Get(&con, query)
	return con, err
}

func (d *block) StrippedSize() UInt64 {
	return UInt64(d.block.StrippedSize)
}

func (d *block) Size() UInt64 {
	return UInt64(d.block.Size)
}

func (d *block) Weight() UInt64 {
	return UInt64(d.block.Weight)
}

func (d *block) Height() UInt64 {
	return UInt64(d.block.Height)
}

func (d *block) Version() UInt64 {
	return UInt64(d.block.Version)
}

func (d *block) VersionHex() string {
	return d.block.VersionHex
}

func (d *block) MerkleRoot() string {
	return d.block.MerkleRoot
}

func (d *block) Time() graphql.Time {
	t := time.Unix(d.block.Time, 0)
	return graphql.Time{t}
}

func (d *block) MedianTime() graphql.Time {
	t := time.Unix(d.block.MedianTime, 0)
	return graphql.Time{t}
}

func (d *block) Nonce() Int64 {
	return Int64(d.block.Nonce)
}

func (d *block) Bits() string {
	return d.block.Bits
}

func (d *block) Difficulty() float64 {
	return float64(d.block.Difficulty)
}

func (d *block) Chainwork() string {
	return d.block.Chainwork
}

func (d *block) PreviousBlockhash() string {
	return d.block.PreviousBlockhash
}

func (d *block) NextBlockhash() string {
	return d.block.NextBlockhash
}

func (d *block) Transactions() (*[]*transaction, error) {
	q := squirrel.Select("*").From("transactions").Where(squirrel.Eq{"block_hash": d.Hash()})
	query, arg, _ := q.PlaceholderFormat(squirrel.Dollar).ToSql()

 	var trxs []*dbTransaction
	if err := d.db.Select(&trxs, query, arg...); err != nil {
		return nil, err
	}

	var res []*transaction
	for _, t := range trxs {
		res = append(res, &transaction{
			db: d.db,
			trx: t,
		})
	}

	return &res, nil
}