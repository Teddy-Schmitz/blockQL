package graph

import (
	"github.com/jmoiron/sqlx"
	"encoding/base64"
	"fmt"
	"github.com/Masterminds/squirrel"
)

type BlockEdge struct {
	db *sqlx.DB

	block *block
}

func (b *BlockEdge) Node() *block {
	return b.block
}

func (b *BlockEdge) Cursor() string {
	return base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%v", b.block.Height())))
}

type blockConnection struct {
	db *sqlx.DB

	args *blocksArgs
	query squirrel.SelectBuilder
}

func (b *blockConnection) Count() (int32, error){
	query, arg, _ := squirrel.Select("count(height)").
		FromSelect(b.query, "x").PlaceholderFormat(squirrel.Dollar).ToSql()

		var count int32
		if err := b.db.Get(&count, query, arg...); err != nil {
			return 0, err
		}
		return count, nil
}

func (b *blockConnection) Edges() (*[]*BlockEdge, error) {

	q := b.query

	if b.args.After != nil {
		h, err := base64.StdEncoding.DecodeString(*b.args.After)
		if err != nil {
			return nil, err
		}
		q = q.Where(squirrel.Gt{"height": string(h)})
	}


	query, arg, _ := q.Limit(uint64(b.args.First)).
		PlaceholderFormat(squirrel.Dollar).ToSql()

	var blocks []*dbBlock
	if err := b.db.Select(&blocks, query, arg...); err != nil {
		return nil, err
	}

	var edges []*BlockEdge
	for _, bl := range blocks {
		edges = append(edges, &BlockEdge{db: b.db, block: &block{
			block: bl,
			db: b.db,
		}})
	}

	return &edges, nil
}

func (b *blockConnection) PageInfo() (*PageInfo) {

	return &PageInfo{
		hasprevious: false,
		hasnext: true,
	}
}

