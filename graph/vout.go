package graph

import (
	"github.com/jmoiron/sqlx"
	sq "github.com/Masterminds/squirrel"
)

type vOut struct {
	db *sqlx.DB

	trx *dbVout
}

type dbVout struct {
	UID int64 `json:"uid" db:"uid"`
	Value string `json:"value" db:"value"`
	Index int64 `json:"n" db:"index" db:"index"`
	ASM *string `json:"asm,omitempty" db:"asm"`
	HEX *string `json:"hex,omitempty" db:"hex"`
	Type *string `json:"type,omitempty" db:"type"`
	ReqSigs *int64 `json:"reqSigs,omitempty" db:"req_sigs"`
	Address *string `json:"address,omitempty" db:"address"`

	TransactionID string `json:"transaction_id" db:"transaction_id"`
}

func (d *vOut) UID() Int64 {
	return Int64(d.trx.UID)
}

func (d *vOut) Value() string {
	return d.trx.Value
}

func (d *vOut) Index() Int64 {
	return Int64(d.trx.Index)
}

func (d *vOut) ASM() *string {
	return d.trx.ASM
}

func (d *vOut) HEX() *string {
	return d.trx.HEX
}

func (d *vOut) Type() *string {
	return d.trx.Type
}

func (d *vOut) ReqSigs() *Int64 {
	if d.trx.ReqSigs == nil {
		return nil
	}
	t := Int64(*d.trx.ReqSigs)
	return &t
}

func (d *vOut) Address() *string {
	return d.trx.Address
}

func (d *vOut) TransactionID() string {
	return d.trx.TransactionID
}

func (d *vOut) Transaction() (*transaction, error) {
	q := sq.Select("*").From("transactions").Where(sq.Eq{"id": d.TransactionID()})
	trx := &dbTransaction{}
	query, arg, _ := q.PlaceholderFormat(sq.Dollar).ToSql()

	if err := d.db.Get(trx, query, arg...); err != nil {
		return nil ,err
	}

	return &transaction{
		db: d.db,
		trx: trx,
	}, nil
}