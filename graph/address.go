package graph

import (
	"github.com/jmoiron/sqlx"
	"database/sql"
	sq "github.com/Masterminds/squirrel"
)

type address struct {
	db *sqlx.DB

	address string
}

type trxsArgs struct {
	First int32
	After *string
}

func (a *address) Transactions(args trxsArgs) (*transactionConnection, error){

	q := sq.Select("trx.*").From("transaction_out trxo").
		Join("transactions trx on trx.id=trxo.transaction_id").Where("address = ?", a.address).OrderBy("trx.time")


	return &transactionConnection{
		query: q,
		db: a.db,
		args: &args,
	}, nil
}

func (a *address) Spends(args trxsArgs) (*vinConnection, error){
	q := sq.Select("txi.*").From("transaction_out txo").
		Join("transaction_in txi on txi.previous_transaction_id = txo.transaction_id AND txo.index=txi.out_index").
			Where("txo.address = ?", a.address)

	return &vinConnection{
		db: a.db,
		query: q,
		args: &args,
	}, nil
}

func (a *address) Receives(args trxsArgs) (*voutConnection, error){
	q := sq.Select("txo.*").From("transaction_out txo").
			Where("txo.address = ?", a.address)

	return &voutConnection{
		db: a.db,
		query: q,
		args: &args,
	}, nil
}

func (a *address) Value() (string, error){
	q := a.db.Rebind(`
SELECT 
	SUM(txo.value)
FROM transaction_out txo 
LEFT JOIN transaction_in txi on txo.transaction_id = txi.previous_transaction_id and txo.index = txi.out_index
WHERE txo.address = ? AND txi.sequence IS NULL GROUP BY txo.address LIMIT 1`)

	var val string
	if err := a.db.Get(&val, q, a.address); err != nil {
		if err == sql.ErrNoRows {
			return "0", nil
		}
		return "", err
	}

	return val, nil
}

func (a *address) Address() string {
	return a.address
}

func (a *address) VerifiedAddress() (*string, error) {
	q := sq.Select("1").From("transaction_out txo").Where("txo.address = ?", a.address).Limit(1)
	query, arg, _ := q.PlaceholderFormat(sq.Dollar).ToSql()

	var count int32
	if err := a.db.Get(&count, query, arg...); err != nil {
		return nil, err
	}

	return &a.address, nil
}