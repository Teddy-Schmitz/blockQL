package graph

import (
	"github.com/jmoiron/sqlx"
	"encoding/base64"
	"fmt"
	"github.com/Masterminds/squirrel"
)

type TransactionEdge struct {
	db *sqlx.DB

	transaction *transaction
}

func (b *TransactionEdge) Node() *transaction {
	return b.transaction
}

func (b *TransactionEdge) Cursor() string {
	return base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%v", b.transaction.UID())))
}

type transactionConnection struct {
	db *sqlx.DB

	args *trxsArgs
	query squirrel.SelectBuilder
}

func (b *transactionConnection) Count() (int32, error){
	query, arg, _ := squirrel.Select("count(id)").
		FromSelect(b.query, "x").PlaceholderFormat(squirrel.Dollar).ToSql()

		var count int32
		if err := b.db.Get(&count, query, arg...); err != nil {
			return 0, err
		}
		return count, nil
}

func (b *transactionConnection) Edges() (*[]*TransactionEdge, error) {

	q := b.query

	if b.args.After != nil {
		h, err := base64.StdEncoding.DecodeString(*b.args.After)
		if err != nil {
			return nil, err
		}
		q = q.Where(squirrel.GtOrEq{"uid": string(h)})
	}

	query, arg, _ := q.Limit(uint64(b.args.First)).
		PlaceholderFormat(squirrel.Dollar).ToSql()

	var trxs []*dbTransaction
	if err := b.db.Select(&trxs, query, arg...); err != nil {
		return nil, err
	}

	var edges []*TransactionEdge
	for _, trx := range trxs {
		edges = append(edges, &TransactionEdge{db: b.db, transaction: &transaction{
			trx: trx,
			db: b.db,
		}})
	}

	return &edges, nil
}

func (b *transactionConnection) PageInfo() (*PageInfo) {

	return &PageInfo{
		hasprevious: false,
		hasnext: true,
	}
}

