package graph

import (
	"github.com/jmoiron/sqlx"
	"encoding/base64"
	"fmt"
	"github.com/Masterminds/squirrel"
)

type VOutEdge struct {
	db *sqlx.DB

	trx *vOut
}

func (b *VOutEdge) Node() *vOut {
	return b.trx
}

func (b *VOutEdge) Cursor() string {
	return base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%v", b.trx.UID())))
}

type voutConnection struct {
	db *sqlx.DB

	args *trxsArgs
	query squirrel.SelectBuilder
}

func (b *voutConnection) Count() (int32, error){
	query, arg, _ := squirrel.Select("count(uid)").
		FromSelect(b.query, "x").PlaceholderFormat(squirrel.Dollar).ToSql()

		var count int32
		if err := b.db.Get(&count, query, arg...); err != nil {
			return 0, err
		}
		return count, nil
}

func (b *voutConnection) Edges() (*[]*VOutEdge, error) {

	q := b.query.OrderBy("txo.uid")

	if b.args.After != nil {
		h, err := base64.StdEncoding.DecodeString(*b.args.After)
		if err != nil {
			return nil, err
		}
		q = q.Where(squirrel.GtOrEq{"txo.uid": string(h)})
	}

	query, arg, _ := q.Limit(uint64(b.args.First)).
		PlaceholderFormat(squirrel.Dollar).ToSql()

	var trxs []*dbVout
	if err := b.db.Select(&trxs, query, arg...); err != nil {
		return nil, err
	}

	var edges []*VOutEdge
	for _, trx := range trxs {
		edges = append(edges, &VOutEdge{db: b.db, trx: &vOut{
			trx: trx,
			db: b.db,
		}})
	}

	return &edges, nil
}

func (b *voutConnection) PageInfo() (*PageInfo) {

	return &PageInfo{
		hasprevious: false,
		hasnext: true,
	}
}

