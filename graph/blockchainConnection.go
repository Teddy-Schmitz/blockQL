package graph

import (
	"github.com/jmoiron/sqlx"
	sq "github.com/Masterminds/squirrel"
	"git.egnomatics.com/jdioutkast/goBlocky/rpcclient"
	"github.com/spf13/viper"
	"net/http"
	"fmt"
	"encoding/json"
	"github.com/pkg/errors"
)

type blockchainConnection struct {
	db *sqlx.DB
	args *blockchainArgs
}

type richList struct {
	DBaddress string `db:"address"`
	DBvalue string `db:"value"`
	DBrank int32 `db:"rank"`
}

func (r *richList) Address() string {
	return r.DBaddress
}

func (r *richList) Value() string {
	return r.DBvalue
}

func (r *richList) Rank() int32 {
	return r.DBrank
}

func (b *blockchainConnection) RichList() (*[]*richList, error){
	q := sq.Select("address", "value", "rank() OVER (ORDER BY value DESC) as rank").From("richlist").Limit(100)
	//q := sq.Select("address", "sum(txo.value) as value").From("transaction_out txo").
	//	LeftJoin("transaction_in txi on txo.transaction_id = txi.previous_transaction_id and txo.index = txi.out_index").
	//Where(sq.Eq{"txi.sequence": nil}).GroupBy("address").OrderBy("value DESC").Limit(uint64(b.args.Limit))

	query,arg,_ := q.PlaceholderFormat(sq.Dollar).ToSql()

	var res []*richList
	if err := b.db.Select(&res, query, arg...); err != nil {
		return nil, err
	}

 return &res, nil
}

type recentArgs struct {
	Limit int32
}

func (b *blockchainConnection) RecentBlocks(args recentArgs) (*[]*block, error){
	q := sq.Select("*").From("blocks").OrderBy("height DESC").Limit(uint64(args.Limit))

	query, arg, _ := q.PlaceholderFormat(sq.Dollar).ToSql()
	var blocks []*dbBlock
	if err := b.db.Select(&blocks, query, arg...); err != nil {
		return nil, err
	}

	var res []*block
	for _, bl := range blocks {
		res = append(res, &block {
			block: bl,
			db: b.db,
		})
	}

	return &res, nil
}

//TODO: Calculate this using SQL
func (b *blockchainConnection) NetworkHash() (string, error) {
	client := rpcclient.Client{
		Host: viper.GetString("daemon"),
		User: viper.GetString("rpcuser"),
		Password: viper.GetString("rpcpassword"),
		Client: http.DefaultClient,
	}

	res, err := client.GetMiningInfo()
	if err != nil {
		return "", err
	}

	return res.NetworkHash.String(), nil
}

func (b *blockchainConnection) CoinSupply() (string, error) {
	q := b.db.Rebind(`
SELECT 
	SUM(txo.value)
FROM transaction_out txo 
LEFT JOIN transaction_in txi on txo.transaction_id = txi.previous_transaction_id and txo.index = txi.out_index
WHERE txi.sequence IS NULL LIMIT 1`)

	var val string
	if err := b.db.Get(&val, q); err != nil {
		return "", err
	}

	return val, nil
}

type CoinMarketResponse struct {
	PriceUSD string `json:"price_usd"`
}

func (b *blockchainConnection) Price() (string, error) {
	if viper.GetString("coinmarket-id") != "" {
		res, err := http.Get(fmt.Sprintf("https://api.coinmarketcap.com/v1/ticker/%s", viper.GetString("coinmarket-id")))
		if err != nil {
			return "", err
		}
		defer res.Body.Close()
		cr := []*CoinMarketResponse{}
		if err := json.NewDecoder(res.Body).Decode(&cr); err != nil {
			return "", err
		}

		if len(cr) > 0 {
			return cr[0].PriceUSD, nil
		}
	}

	return "", errors.New("No/Invalid coin id specified")
}