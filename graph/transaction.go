package graph

import (
	"github.com/jmoiron/sqlx"
	"github.com/Masterminds/squirrel"
	"github.com/neelance/graphql-go"
	"time"
)

type transaction struct {
	db *sqlx.DB

	trx *dbTransaction
	query squirrel.SelectBuilder
}

type dbTransaction struct {
	UID int64 `json:"uid" db:"uid"`
	ID string `json:"txid" db:"id"`
	Hash string `json:"hash" db:"hash"`
	Version int64 `json:"version" db:"version"`
	Size uint64 `json:"size" db:"size"`
	Vsize uint64 `json:"vsize" db:"vsize"`
	Locktime uint64 `json:"locktime" db:"locktime"`
	BlockHash string `json:"blockhash" db:"block_hash"`
	Time int64 `json:"time" db:"time"`
}

func (d *transaction) UID() Int64 {
	return Int64(d.trx.UID)
}

func (d *transaction) ID() string {
	return d.trx.ID
}

func (d *transaction) Hash() string {
	return d.trx.Hash
}

func (d *transaction) Locktime() UInt64 {
	return UInt64(d.trx.Locktime)
}

func (d *transaction) Size() UInt64 {
	return UInt64(d.trx.Size)
}

func (d *transaction) Vsize() UInt64 {
	return UInt64(d.trx.Vsize)
}

func (d *transaction) Version() UInt64 {
	return UInt64(d.trx.Version)
}

func (d *transaction) BlockHash() string {
	return d.trx.BlockHash
}

func (d *transaction) Time() graphql.Time {
	t := time.Unix(d.trx.Time, 0)
	return graphql.Time{t}
}

func (d *transaction) Block() (*block, error) {
	b := &dbBlock{}
	q := squirrel.Select("*").From("blocks").Where(squirrel.Eq{"hash": d.BlockHash()})
	query, arg, _ := q.PlaceholderFormat(squirrel.Dollar).ToSql()
	err := d.db.Get(b, query, arg...)

	return &block{
		block: b,
		db: d.db,
	}, err
}

func (d *transaction) VIn(args trxsArgs) (*vinConnection, error) {
	q := squirrel.Select("*").From("transaction_in txi").Where(squirrel.Eq{"transaction_id": d.ID()})
	return &vinConnection{
		db: d.db,
		args: &args,
		query: q,
	}, nil
}

func (d *transaction) VOut(args trxsArgs) (*voutConnection, error) {
	q := squirrel.Select("*").From("transaction_out txo").Where(squirrel.Eq{"transaction_id": d.ID()})
	return &voutConnection{
		db: d.db,
		args: &args,
		query: q,
	}, nil
}

func (d *transaction) Value() (string, error){
	q := squirrel.Select("sum(value)").From("transaction_out txo").Where(squirrel.Eq{"transaction_id": d.ID()})
	query, arg, _ := q.PlaceholderFormat(squirrel.Dollar).ToSql()
	var res string
	if err := d.db.Get(&res, query, arg...); err != nil {
		return "", err
	}

	return res, nil
}