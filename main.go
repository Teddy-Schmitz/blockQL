package main

import (
	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	flag "github.com/spf13/pflag"
	"github.com/spf13/viper"
	"fmt"
	"github.com/fsnotify/fsnotify"

	_ "github.com/lib/pq"
	"net/http"
	"github.com/gorilla/handlers"
	"git.egnomatics.com/jdioutkast/blockQL/graph"
	"git.egnomatics.com/jdioutkast/blockQL/db"
	"context"
	"time"
)


func CORS() func(http.Handler) http.Handler {
	allowedHeaders := handlers.AllowedHeaders([]string{"Content-Type"})
	allowedMethods := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT"})

	return handlers.CORS(allowedHeaders, allowedMethods, handlers.AllowCredentials(), handlers.AllowedOrigins([]string{"*"}))
}

func main() {

	databaseSource := fmt.Sprintf("%v://%v:%v@%v/%v?sslmode=disable",
		viper.GetString("db-driver"),
		viper.GetString("db-username"),
		viper.GetString("db-password"),
		viper.GetString("db-host"),
		viper.GetString("db-database"))

	logrus.Debugln("Connecting to DB")
	DB, err := sqlx.Open(viper.GetString("db-driver"), databaseSource)
	if err != nil {
		logrus.Fatalln(err)
	}

	ctx, _ := context.WithTimeout(context.Background(), 30 * time.Second)
	if err := DB.PingContext(ctx); err != nil {
		logrus.WithError(err).Fatalln("failed to ping connection")
	}

	logrus.Debugln("DB connected")

	DB.SetMaxOpenConns(4 * viper.GetInt("concurrency"))
	DB.SetMaxIdleConns(4 * viper.GetInt("concurrency"))


	refresh := &db.RefreshContext{
		DB: DB,
	}

	if (viper.GetBool("refresh")) {
		go db.RefreshRichList(context.Background(), DB)
	}
	go refresh.RefreshWorker()

	r := mux.NewRouter()
	graph.RegisterGraphQLRoutes(r.PathPrefix("/graphql").Subrouter(), DB)
	http.ListenAndServe(":5050", CORS()(r))
}


func init() {

	flag.StringP( "daemon", "d", "", "Address and Port of coin daemon to use")
	flag.StringP("rpcuser", "u", "", "JSON-RPC User")
	flag.StringP("rpcpassword", "p", "", "JSON-RPC Password")

	flag.String("db-driver", "postgres", "Database drive name")
	flag.String("db-username", "", "Database username")
	flag.String("db-password", "", "Database password")
	flag.String("db-host", "localhost", "Database host")
	flag.String("db-database", "blocky", "Database name")
	flag.BoolP("refresh", "r", false, "Refresh DB views on startup")

	flag.Bool("debug", false, "Enable debug logging")
	flag.Bool("profiler", false, "Enable pprof")
	flag.Bool("trace", false, "Enable tracing")

	flag.Int("concurrency", 4, "How many goroutines to run at once")

	flag.String("coinmarket-id", "", "CoinMarketCap ID")

	viper.SetConfigName("blockql")
	viper.AddConfigPath("$HOME/.blockql")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()

	if err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); !ok{
			logrus.Fatalln(err)
		}
	} else {
		viper.OnConfigChange(func(in fsnotify.Event){
			logrus.Printf("configuration reloaded")
		})
		viper.WatchConfig()
	}

	viper.AutomaticEnv()

	flag.Parse()
	viper.BindPFlags(flag.CommandLine)
}