CREATE MATERIALIZED VIEW
  richlist
AS
  SELECT
    address,
    sum(txo.value) AS value
  FROM transaction_out txo
    LEFT JOIN transaction_in txi
      ON txi.previous_transaction_id = txo.transaction_id AND txo.index = txi.out_index
  WHERE txi.sequence IS NULL GROUP BY address ORDER BY value DESC;

CREATE UNIQUE INDEX ON richlist(address);