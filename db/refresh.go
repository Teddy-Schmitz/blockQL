package db

import (
	"github.com/jmoiron/sqlx"
	"context"
	"time"
	"github.com/sirupsen/logrus"
)

type RefreshContext struct {
	DB *sqlx.DB
}

func RefreshRichList(ctx context.Context, DB *sqlx.DB) error {
	q := "REFRESH MATERIALIZED VIEW CONCURRENTLY richlist"
	_, err := DB.ExecContext(ctx, q)
	if err != nil {
		return err
	}
	return nil
}

func (r *RefreshContext) RefreshWorker() {
	t := time.NewTicker(2 * time.Hour)
	for range t.C {
		ctx, _ := context.WithTimeout(context.Background(), 5*time.Minute)
		err := RefreshRichList(ctx, r.DB)
		if err != nil {
			logrus.WithError(err).Errorln("unable to refresh richlist")
		}
	}
}


